# Import datasets to package
# 2023-05-11

# Load packages ----

library(tidyverse)
library(vroom)

# Create datasets ----

# Set path
path <- "C:/Users/jkitt/Desktop/Balfourier_et_al_Wheat_Phylogeography_DataS2/Balfourier_et_al_Wheat_Phylogeography_DataS2.tab"

# Import dataset
d1 <- vroom::vroom(path) |>
  dplyr::rename(probeset_id = psId,
                snp_id = snpId,
                position = snpPosition,
                quality = snpQuality)

# Genotypes
genotypes <- d1 |>
  dplyr::select(-c(snp_id, quality))

genotypes_chr1A <- genotypes |>
  dplyr::filter(chromosome == "chr1A")

genotypes_chr2A <- genotypes |>
  dplyr::filter(chromosome == "chr2A")

genotypes_chr3A <- genotypes |>
  dplyr::filter(chromosome == "chr3A")

genotypes_chr4A <- genotypes |>
  dplyr::filter(chromosome == "chr4A")

genotypes_chr5A <- genotypes |>
  dplyr::filter(chromosome == "chr5A")

genotypes_chr6A <- genotypes |>
  dplyr::filter(chromosome == "chr6A")

genotypes_chr7A <- genotypes |>
  dplyr::filter(chromosome == "chr7A")

genotypes_chr1B <- genotypes |>
  dplyr::filter(chromosome == "chr1B")

genotypes_chr2B <- genotypes |>
  dplyr::filter(chromosome == "chr2B")

genotypes_chr3B <- genotypes |>
  dplyr::filter(chromosome == "chr3B")

genotypes_chr4B <- genotypes |>
  dplyr::filter(chromosome == "chr4B")

genotypes_chr5B <- genotypes |>
  dplyr::filter(chromosome == "chr5B")

genotypes_chr6B <- genotypes |>
  dplyr::filter(chromosome == "chr6B")

genotypes_chr7B <- genotypes |>
  dplyr::filter(chromosome == "chr7B")

genotypes_chr1D <- genotypes |>
  dplyr::filter(chromosome == "chr1D")

genotypes_chr2D <- genotypes |>
  dplyr::filter(chromosome == "chr2D")

genotypes_chr3D <- genotypes |>
  dplyr::filter(chromosome == "chr3D")

genotypes_chr4D <- genotypes |>
  dplyr::filter(chromosome == "chr4D")

genotypes_chr5D <- genotypes |>
  dplyr::filter(chromosome == "chr5D")

genotypes_chr6D <- genotypes |>
  dplyr::filter(chromosome == "chr6D")

genotypes_chr7D <- genotypes |>
  dplyr::filter(chromosome == "chr7D")

usethis::use_data(genotypes_chr1A, overwrite = TRUE)
usethis::use_data(genotypes_chr2A, overwrite = TRUE)
usethis::use_data(genotypes_chr3A, overwrite = TRUE)
usethis::use_data(genotypes_chr4A, overwrite = TRUE)
usethis::use_data(genotypes_chr5A, overwrite = TRUE)
usethis::use_data(genotypes_chr6A, overwrite = TRUE)
usethis::use_data(genotypes_chr7A, overwrite = TRUE)
usethis::use_data(genotypes_chr1B, overwrite = TRUE)
usethis::use_data(genotypes_chr2B, overwrite = TRUE)
usethis::use_data(genotypes_chr3B, overwrite = TRUE)
usethis::use_data(genotypes_chr4B, overwrite = TRUE)
usethis::use_data(genotypes_chr5B, overwrite = TRUE)
usethis::use_data(genotypes_chr6B, overwrite = TRUE)
usethis::use_data(genotypes_chr7B, overwrite = TRUE)
usethis::use_data(genotypes_chr1D, overwrite = TRUE)
usethis::use_data(genotypes_chr2D, overwrite = TRUE)
usethis::use_data(genotypes_chr3D, overwrite = TRUE)
usethis::use_data(genotypes_chr4D, overwrite = TRUE)
usethis::use_data(genotypes_chr5D, overwrite = TRUE)
usethis::use_data(genotypes_chr6D, overwrite = TRUE)
usethis::use_data(genotypes_chr7D, overwrite = TRUE)

# List of samples
samples <- vroom::vroom("../../../projects/breedwheat/data/samples_lists/Balfourier_et_al_Wheat_Phylogeography_DataS1.txt")

d1 <- samples |>
  dplyr::mutate(sample_code = paste0("ERGE", ID_code), .before = accession_name)

d2 <- d1 |>
  dplyr::select(sample_code, sample_name = accession_name)

new_row <- tibble(sample_code = "ERGE181",
                         sample_name = "NA")

d3 <- rbind(d2, new_row)
d3

erge_list <- names(wheatdata::genotypes_chr1A)[-c(1:3)]

samples <- d3 |>
  dplyr::filter(sample_code != "ERGE183") |>
  dplyr::arrange(factor(sample_code, levels = erge_list)) |>
  dplyr::slice(1:4506)

usethis::use_data(samples, overwrite = TRUE)
