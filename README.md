# wheatdata

R package containing genotyping data from the Balfourier et al. paper [Worldwide phylogeography and history of wheat genetic diversity](https://doi.org/10.1126/sciadv.aav0536) founded in the scope of BreedWheat project.


## Download the package

To download the development version of the *wheatdata* package, use the following code: `devtools::install_git("https://forgemia.inra.fr/umr-gdec/wheatdata")`

## Accessing the data

### List of samples

To access the list of 4,506 samples, use the following code:
`samples`

### For a given chromosome

To access the data for a given chromosome, use the following code:

`genotypes_chr1A`

### Extract data

The `get_geno()` function can be used to extract specific data.

| Chromosomes              | Samples              | Code                                                                             |
|--------------------------|----------------------|----------------------------------------------------------------------------------|
| All chromosomes          | All samples          | `get_geno()`                                                                     |
| All chromosomes          | Selection of samples | `get_geno(codes = c("ERGE1019", "ERGE1254")`                                     |
| Selection of chromosomes | All samples          | `get_geno(chr = c("chr5D", "chr6D", "chr7D"))`                                   |
| Selection of chromosomes | Selection of samples | `get_geno(chr = c("chr5D", "chr6D", "chr7D"), codes = c("ERGE1019", "ERGE1254")` |
| Subgenome                | All samples          | `get_geno(genome = "A")`                                                         |
| Homoeologs               | All samples          | `get_geno(chr_nb = 1)`                                                           |

## Citation

```bibtex
@article{
doi:10.1126/sciadv.aav0536,
author = {François Balfourier  and Sophie Bouchet  and Sandra Robert  and Romain De Oliveira  and Hélène Rimbert  and Jonathan Kitt  and Frédéric Choulet  and International Wheat Genome Sequencing Consortium and BreedWheat Consortium and Etienne Paux },
title = {Worldwide phylogeography and history of wheat genetic diversity},
journal = {Science Advances},
volume = {5},
number = {5},
pages = {eaav0536},
year = {2019},
doi = {10.1126/sciadv.aav0536},
URL = {https://www.science.org/doi/abs/10.1126/sciadv.aav0536},
eprint = {https://www.science.org/doi/pdf/10.1126/sciadv.aav0536},
abstract = {A phylogeographical study reveals the reshuffling of wheat genetic diversity through time, in relation to human history. Since its domestication in the Fertile Crescent ~8000 to 10,000 years ago, wheat has undergone a complex history of spread, adaptation, and selection. To get better insights into the wheat phylogeography and genetic diversity, we describe allele distribution through time using a set of 4506 landraces and cultivars originating from 105 different countries genotyped with a high-density single-nucleotide polymorphism array. Although the genetic structure of landraces is collinear to ancient human migration roads, we observe a reshuffling through time, related to breeding programs, with the appearance of new alleles enriched with structural variations that may be the signature of introgressions from wild relatives after 1960.}}

```

## RDG repository

The raw datasets are also available on Recherche-Data-Gouv portal : [https://doi.org/10.57745/WX6QOY](https://doi.org/10.57745/WX6QOY)

